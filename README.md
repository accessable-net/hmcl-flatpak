Flatpak of [HMCL](https://github.com/huanghongxun/HMCL), a Minecraft launcher which supports Mod management, game customizing, auto installing(Forge, LiteLoader and OptiFine), modpack creating, UI customizing and so on.

> Install *org.freedesktop.Sdk.Extension.openjdk8*, if you want to run minecraft older than 1.17.  

## How to install

````bash
flatpak install --user https://gitlab.com/accessable-net/hmcl-flatpak/raw/master/hmcl.flatpakref
````
